This is repository to setup internet sharing for linux based os.  
NOTE: this tutorial consider routing client access to eth0 to use internet from wlan0

1. Included is the scrip to setup everything.  
but before that required to manually edit some configuration

2. install dnsmasq  
  sudo apt-get update  
  sudo apt-get install dnsmasq  
  yes to add /etc/dnsmasq.conf  

3. setup static ip for eth0 makesure no clash. and also setup wlan0 as required....  
  vim /etc/network/interface  
    set static for example  
    auto eth0  
    iface eth0 inet static  

4. enable ip_forwarding.  
  vim /etc/sysctl.conf  
   uncomment or add this line  
   net.ipv4.ip_forward=1  
   (require soft reboot to apply)  

5. execute the script or add to automatic run on boot  
   copy ".service" file to /etc/systemd/system/.  
   then test it by  
   start:     systemctl start <service name>  
   status:      systemctl status <service name>  
   stop:      systemctl stop <service name>  

   then enable the service  
   systemctl enable <service name>  
